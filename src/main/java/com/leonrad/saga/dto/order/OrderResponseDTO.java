package com.leonrad.saga.dto.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderResponseDTO {

    private int id;
    private int userId;
    private int productId;
    private OrderStatus orderStatus;

}
