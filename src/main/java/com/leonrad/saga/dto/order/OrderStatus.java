package com.leonrad.saga.dto.order;

public enum OrderStatus {
    CREATED,
    COMPLETED,
    FAILED
}
