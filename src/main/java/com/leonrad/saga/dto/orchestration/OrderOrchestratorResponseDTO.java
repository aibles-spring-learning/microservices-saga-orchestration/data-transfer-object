package com.leonrad.saga.dto.orchestration;

import com.leonrad.saga.dto.order.OrderStatus;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderOrchestratorResponseDTO {
    private int userId;
    private int orderId;
    private int productId;
    private OrderStatus orderStatus;
}
