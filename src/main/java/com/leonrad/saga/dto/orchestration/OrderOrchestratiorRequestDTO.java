package com.leonrad.saga.dto.orchestration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderOrchestratiorRequestDTO implements Serializable {
    private int userId;
    private int orderId;
    private int productId;
}
