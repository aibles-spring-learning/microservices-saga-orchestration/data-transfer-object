package com.leonrad.saga.dto.inventory;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InventoryRequestDTO {
    private int productId;
    private int orderId;
}
