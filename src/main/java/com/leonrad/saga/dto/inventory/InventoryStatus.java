package com.leonrad.saga.dto.inventory;

public enum InventoryStatus {
    AVAILABLE,
    OUT_OF_STOCK
}
